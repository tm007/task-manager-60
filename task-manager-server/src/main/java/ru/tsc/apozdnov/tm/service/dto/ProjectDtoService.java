package ru.tsc.apozdnov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.apozdnov.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.apozdnov.tm.api.service.dto.IProjectDtoDtoService;
import ru.tsc.apozdnov.tm.dto.model.ProjectDtoModel;
import ru.tsc.apozdnov.tm.exception.field.EmptyDescriptionException;
import ru.tsc.apozdnov.tm.exception.field.EmptyNameException;
import ru.tsc.apozdnov.tm.exception.field.EmptyUserIdException;

import java.util.Date;

@Service
public class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDtoModel, IProjectDtoRepository>
        implements IProjectDtoDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @NotNull
    @Override
    protected IProjectDtoRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDtoModel create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable ProjectDtoModel project = new ProjectDtoModel();
        project.setName(name);
        project.setUserId(userId);

        repository.add(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDtoModel create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable ProjectDtoModel project = new ProjectDtoModel();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);

        repository.add(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDtoModel create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable ProjectDtoModel project = new ProjectDtoModel();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);

        repository.add(project);
        return project;
    }

}