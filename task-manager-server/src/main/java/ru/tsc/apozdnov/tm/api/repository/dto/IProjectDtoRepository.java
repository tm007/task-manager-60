package ru.tsc.apozdnov.tm.api.repository.dto;

import ru.tsc.apozdnov.tm.dto.model.ProjectDtoModel;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDtoModel> {

}
