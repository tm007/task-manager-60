package ru.tsc.apozdnov.tm.config;

import org.hibernate.cfg.Environment;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.tsc.apozdnov.tm.api.service.IDatabaseProperty;
import ru.tsc.apozdnov.tm.api.service.IPropertyService;
import ru.tsc.apozdnov.tm.listener.EntityListener;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.tsc.apozdnov.tm")
public class ContextConfig {

    @Bean
    public DataSource dataSource(@NotNull final IPropertyService propertyService) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDatabaseDriver());
        dataSource.setUrl(propertyService.getDatabaseUrl());
        dataSource.setUsername(propertyService.getDatabaseUser());
        dataSource.setPassword(propertyService.getDatabasePassword());
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory,
            @NotNull final EntityListener entityListener
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        @Nullable final EntityManagerFactory factory = entityManagerFactory.getObject();
        initListeners(factory, entityListener);
        transactionManager.setEntityManagerFactory(factory);
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final IDatabaseProperty databaseProperty,
            final DataSource dataSource
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.tsc.apozdnov.tm.model", "ru.tsc.apozdnov.tm.dto");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DRIVER, databaseProperty.getDatabaseDriver());
        properties.put(Environment.URL, databaseProperty.getDatabaseUrl());
        properties.put(Environment.USER, databaseProperty.getDatabaseUser());
        properties.put(Environment.PASS, databaseProperty.getDatabasePassword());
        properties.put(Environment.DIALECT, databaseProperty.getDatabaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHbm2ddlAuto());
        properties.put(Environment.SHOW_SQL, databaseProperty.getDatabaseShowSql());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getUseSecondLevelCache());
        properties.put(Environment.USE_QUERY_CACHE, databaseProperty.getUseQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getUseMinimalPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getCacheRegionPrefix());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getCacheProviderConfig());
        properties.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getCacheRegionFactory());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    private void initListeners(@NotNull final EntityManagerFactory factory, @NotNull final EntityListener entityListener) {
        @NotNull final SessionFactoryImpl sessionFactory = factory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry registry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        registry.getEventListenerGroup(EventType.POST_INSERT).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.POST_DELETE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.PRE_INSERT).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.PRE_DELETE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.PRE_UPDATE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.POST_LOAD).appendListener(entityListener);
    }

}
