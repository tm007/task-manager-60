package ru.tsc.apozdnov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.apozdnov.tm.api.repository.dto.IUserDtoRepository;
import ru.tsc.apozdnov.tm.dto.model.UserDtoModel;

import java.util.List;
import java.util.Optional;

@Repository
public class UserDtoRepository extends AbstractDtoRepository<UserDtoModel> implements IUserDtoRepository {

    @NotNull
    @Override
    public List<UserDtoModel> findAll() {
        @NotNull final String jpql = "SELECT m FROM UserDtoModel m";
        return entityManager.createQuery(jpql, UserDtoModel.class).getResultList();
    }

    @Nullable
    @Override
    public UserDtoModel findOneById(@NotNull String id) {
        return entityManager.find(UserDtoModel.class, id);
    }

    @Override
    public void removeById(@NotNull String id) {
        Optional<UserDtoModel> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM UserDtoModel";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public long getCount() {
        @NotNull final String jpql = "SELECT COUNT(e) FROM UserDtoModel e";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM UserDtoModel m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public UserDtoModel findOneByLogin(@NotNull String login) {
        @NotNull final String jpql = "SELECT m FROM UserDtoModel m WHERE m.login = :login";
        return entityManager.createQuery(jpql, UserDtoModel.class)
                .setParameter("login", login)
                .setHint("org.hibernate.cacheable", true)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDtoModel findOneByEmail(@NotNull String email) {
        @NotNull final String jpql = "SELECT m FROM UserDtoModel m WHERE m.email = :email";
        return entityManager.createQuery(jpql, UserDtoModel.class)
                .setParameter("email", email)
                .setHint("org.hibernate.cacheable", true)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

}