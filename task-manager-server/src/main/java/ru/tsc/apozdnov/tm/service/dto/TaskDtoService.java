package ru.tsc.apozdnov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.apozdnov.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.apozdnov.tm.api.service.dto.ITaskDtoService;
import ru.tsc.apozdnov.tm.dto.model.TaskDtoModel;
import ru.tsc.apozdnov.tm.exception.field.EmptyDescriptionException;
import ru.tsc.apozdnov.tm.exception.field.EmptyIdException;
import ru.tsc.apozdnov.tm.exception.field.EmptyNameException;
import ru.tsc.apozdnov.tm.exception.field.EmptyUserIdException;

import java.util.Date;
import java.util.List;

@Service
public class TaskDtoService extends AbstractUserOwnedDtoService<TaskDtoModel, ITaskDtoRepository> implements ITaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;

    @NotNull
    @Override
    protected ITaskDtoRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @Transactional
    public TaskDtoModel create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable TaskDtoModel task = new TaskDtoModel();
        task.setName(name);
        task.setUserId(userId);
        @NotNull final ITaskDtoRepository repository = getRepository();
        repository.add(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public TaskDtoModel create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable TaskDtoModel task = new TaskDtoModel();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NotNull final ITaskDtoRepository repository = getRepository();
        repository.add(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public TaskDtoModel create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable TaskDtoModel task = new TaskDtoModel();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        @NotNull final ITaskDtoRepository repository = getRepository();
        repository.add(task);
        return task;
    }

    @NotNull
    @Override
    public List<TaskDtoModel> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository repository = getRepository();
        return repository.findAllByProjectId(userId, projectId);
    }

}