package ru.tsc.apozdnov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.dto.model.TaskDtoModel;

import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDtoModel> {

    @NotNull
    List<TaskDtoModel> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTasksByProjectId(String projectId);

}