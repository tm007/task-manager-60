package ru.tsc.apozdnov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.UserDtoModel;

public interface IUserDtoRepository extends IDtoRepository<UserDtoModel> {

    @Nullable
    UserDtoModel findOneByLogin(@NotNull String login);

    @Nullable
    UserDtoModel findOneByEmail(@NotNull String email);

}