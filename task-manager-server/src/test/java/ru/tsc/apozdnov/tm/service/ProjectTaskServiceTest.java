package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.apozdnov.tm.api.service.dto.IProjectDtoDtoService;
import ru.tsc.apozdnov.tm.api.service.dto.IProjectTaskDtoService;
import ru.tsc.apozdnov.tm.api.service.dto.ITaskDtoService;
import ru.tsc.apozdnov.tm.api.service.dto.IUserDtoService;
import ru.tsc.apozdnov.tm.config.ContextConfig;
import ru.tsc.apozdnov.tm.dto.model.ProjectDtoModel;
import ru.tsc.apozdnov.tm.dto.model.TaskDtoModel;
import ru.tsc.apozdnov.tm.dto.model.UserDtoModel;
import ru.tsc.apozdnov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.EmptyIdException;
import ru.tsc.apozdnov.tm.exception.field.EmptyUserIdException;

public class ProjectTaskServiceTest {

    @NotNull
    private IProjectTaskDtoService projectTaskService;

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private IProjectDtoDtoService projectService;

    @NotNull
    private ITaskDtoService taskService;

    private String USER_ID;

    private String PROJECT_ID;

    private String TASK_ID;

    @Before
    public void init() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ContextConfig.class);
        projectTaskService = context.getBean(IProjectTaskDtoService.class);
        userService = context.getBean(IUserDtoService.class);
        projectService = context.getBean(IProjectDtoDtoService.class);
        taskService = context.getBean(ITaskDtoService.class);
        @NotNull final UserDtoModel user = userService.create("user", "user");
        USER_ID = user.getId();
        @NotNull final ProjectDtoModel project = projectService.create(USER_ID, "project");
        PROJECT_ID = project.getId();
        @NotNull final TaskDtoModel task = taskService.create(USER_ID, "task");
        TASK_ID = task.getId();
    }

    @After
    public void end() {
        taskService.clear(USER_ID);
        projectService.clear(USER_ID);
        userService.removeByLogin("user");
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.bindTaskToProject("", PROJECT_ID, TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, "", TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, "not_task_id"));
        projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, TASK_ID);
        @NotNull final TaskDtoModel task = taskService.findOneById(TASK_ID);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(PROJECT_ID, task.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.unbindTaskFromProject("", PROJECT_ID, TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, "", TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, "not_task_id"));
        projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, TASK_ID);
        @NotNull final TaskDtoModel task = taskService.findOneById(TASK_ID);
        Assert.assertNull(task.getProjectId());
    }

}