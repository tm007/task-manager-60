package ru.tsc.apozdnov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.event.ConsoleEvent;

@Component
public final class ExitListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "exit";

    @NotNull
    public static final String DESCRIPTION = "Close application.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@exitListener.getName() == #event.name")
    public void executeEvent(@NotNull final ConsoleEvent event) {
        System.exit(0);
    }

}