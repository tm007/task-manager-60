package ru.tsc.apozdnov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.event.ConsoleEvent;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

@Component
public final class ProjectStartByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-start-by-id";

    @NotNull
    public static final String DESCRIPTION = "Start project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectStartByIdListener.getName() == #event.name")
    public void executeEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken(), id, Status.IN_PROGRESS);
        getProjectEndpoint().changeProjectStatusById(request);
    }

}